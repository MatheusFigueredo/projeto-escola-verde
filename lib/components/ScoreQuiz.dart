class ScoreQuiz {
  final String _img;
  final String _text;
  final String _title;

  const ScoreQuiz(this._img, this._text, this._title);

  get img => _img;
  get text => _text;
  get title => _title;
}
