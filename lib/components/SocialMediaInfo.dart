class SocialMediaInfo {
  final String _image;
  final String _url;

  SocialMediaInfo(this._image, this._url);

  String get image => this._image;
  String get url=> this._url;
}