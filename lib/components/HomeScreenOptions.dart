class HomeScreenOptions {
  final String _image;
  final String _title;
  final String _route;

  HomeScreenOptions(this._image, this._title,this._route);

  get image => this._image;
  get title => this._title;
  get route => this._route;
}
