class Question {
  final String _img;
  final String _title;
  final String _text;
  final List<Map<String, dynamic>> _options;

  const Question(this._img, this._options, this._text, this._title);

  get img => _img;
  get title => _title;
  get text => _text;
  get options => _options;
}
