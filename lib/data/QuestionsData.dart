import 'package:projeto_escola_verde/components/Question.dart';

const List<Question> QUESTIONS = const [
  question1,
  question2,
  question3,
  question4,
  question5,
  question6,
  question7,
  question8,
  question9,
  question10,
  question11,
  question12,
  question13,
];

const question1 = Question(
  'assets/quiz/question_1.jpg',
  optionsQuestion1,
  'Qual a procedência dos alimentos que consome? ',
  'Questão 1',
);

const List<Map<String, dynamic>> optionsQuestion1 = [
  {'text': 'De minha própria horta', 'score': 0},
  {'text': 'Produzidas localmente', 'score': 2},
  {'text': 'Supermercados e feiras', 'score': 8},
  {'text': 'Somente supermercados', 'score': 10}
];

const question2 = Question(
  'assets/quiz/question_2.jpg',
  optionsQuestion2,
  'Que tipo de meio de transporte costuma usar para locomoção? ',
  'Questão 2',
);

const List<Map<String, dynamic>> optionsQuestion2 = [
  {'text': 'Transporte público ', 'score': 3},
  {'text': 'A pé', 'score': 0},
  {'text': 'Carro ou moto', 'score': 10},
  {'text': 'Bicicleta', 'score': 0}
];

const question3 = Question(
  'assets/quiz/question_3.jpg',
  optionsQuestion3,
  'Quando você vai ao carnaval, são joão, etc. Quais são os cuidados que você tem?',
  'Questão 3',
);

const List<Map<String, dynamic>> optionsQuestion3 = [
  {'text': 'Tomo cuidado com o lixo e emissões que produzo ', 'score': 0},
  {'text': 'Tomo cuidado com o lixo que produzo', 'score': 5},
  {'text': 'Só com as emissões que produzo', 'score': 5},
  {'text': 'Não tenho cuidados ambientais', 'score': 10}
];

const question4 = Question(
  'assets/quiz/question_4.jpg',
  optionsQuestion4,
  'Você se preocupa em reduzir a produção de resíduos?',
  'Questão 4',
);

const List<Map<String, dynamic>> optionsQuestion4 = [
  {'text': 'Sempre', 'score': 0},
  {'text': 'Às vezes', 'score': 3},
  {'text': 'Raramente', 'score': 7},
  {'text': 'Nunca', 'score': 10}
];

const question5 = Question(
  'assets/quiz/question_5.jpg',
  optionsQuestion5,
  'Como é sua alimentação? ',
  'Questão 5',
);

const List<Map<String, dynamic>> optionsQuestion5 = [
  {'text': 'Vegana', 'score': 0},
  {'text': 'Vegetariana', 'score': 3},
  {'text': 'Onívora', 'score': 7},
  {'text': 'Predominantemente carnívora', 'score': 10}
];

const question6 = Question(
  'assets/quiz/question_6.jpg',
  optionsQuestion6,
  'Quando você visita praias. Quais são os cuidados que você tem?',
  'Questão 6',
);

const List<Map<String, dynamic>> optionsQuestion6 = [
  {
    'text': 'Levo uma sacola para o lixo. Uso protetor solar à prova d\'água',
    'score': 0
  },
  {'text': 'Só me preocupo em armazenar o lixo em uma sacola', 'score': 5},
  {'text': 'Não tomo nenhum cuidado', 'score': 10},
  {'text': 'Uso o protetor à prova d\'água', 'score': 5}
];

const question7 = Question(
  'assets/quiz/question_7.jpeg',
  optionsQuestion7,
  'Como costuma lavar sua calçada?',
  'Questão 7',
);

const List<Map<String, dynamic>> optionsQuestion7 = [
  {'text': 'Com mangueira', 'score': 10},
  {'text': 'Com baldes d\'água potável', 'score': 8},
  {'text': 'Com baldes d\'água de chuva', 'score': 0},
  {'text': 'Só varre', 'score': 0}
];

const question8 = Question(
  'assets/quiz/question_8.jpeg',
  optionsQuestion8,
  'Como você descarta o lixo produzido na sua casa?',
  'Questão 8',
);

const List<Map<String, dynamic>> optionsQuestion8 = [
  {'text': 'Não me preocupo com o descarte', 'score': 10},
  {
    'text':
        'Em duas lixeiras, uma para recicláveis e outra para não-recicláveis',
    'score': 4
  },
  {
    'text':
        'Em três lixeiras, uma para recicláveis, outra para não-recicláveis e para baterias.',
    'score': 0
  },
  {'text': 'Em uma única lixeira, pois não tem coleta seletiva', 'score': 6}
];

const question9 = Question(
  'assets/quiz/question_9.jpg',
  optionsQuestion9,
  'Quanto tempo você gasta tomando banho ao longo do dia?',
  'Questão 9',
);

const List<Map<String, dynamic>> optionsQuestion9 = [
  {'text': 'Acima de 30 minutos.', 'score': 10},
  {'text': 'Entre 21 e 25 minutos', 'score': 6},
  {'text': 'Entre 16 e 20 minutos', 'score': 4},
  {'text': 'Entre 5 e 15 minutos', 'score': 0}
];

const question10 = Question(
  'assets/quiz/question_10.jpeg',
  optionsQuestion10,
  'Qual a predominância de lâmpadas  em  sua casa?',
  'Questão 10',
);

const List<Map<String, dynamic>> optionsQuestion10 = [
  {'text': 'São lâmpadas comuns', 'score': 10},
  {'text': 'Minoria são lâmpadas sustentáveis', 'score': 7},
  {'text': 'Maioria são lâmpadas sustentáveis', 'score': 3},
  {'text': 'Todas são sustentáveis', 'score': 0}
];

const question11 = Question(
  'assets/quiz/question_11.jpg',
  optionsQuestion11,
  'Qual o destino do óleo de cozinha usado em sua casa?',
  'Questão 11',
);

const List<Map<String, dynamic>> optionsQuestion11 = [
  {'text': 'É jogado diretamente no ralo.', 'score': 10},
  {'text': 'É doado para empresas que reutilizam o óleo.', 'score': 0},
  {'text': 'Joga o óleo em um terreno baldio.', 'score': 8},
  {
    'text':
        'Você mesmo(a) reutiliza o óleo em casa para produzir sabão, por exemplo.',
    'score': 2
  }
];

const question12 = Question(
  'assets/quiz/question_12.jpg',
  optionsQuestion12,
  'Qual é a fonte de energia predominante na sua casa?',
  'Questão 12',
);

const List<Map<String, dynamic>> optionsQuestion12 = [
  {'text': 'Solar', 'score': 0},
  {'text': 'Eólica', 'score': 2},
  {'text': 'Hidrelétrica', 'score': 6},
  {'text': 'Termoelétrica', 'score': 10}
];
const question13 = Question(
  'assets/quiz/question_13.jpg',
  optionsQuestion13,
  'Qual o destino que você dá ao lixo orgânico?',
  'Questão 13',
);

const List<Map<String, dynamic>> optionsQuestion13 = [
  {'text': 'Jogo no lixo comum', 'score': 10},
  {'text': 'Separo em outra sacola', 'score': 7},
  {'text': 'Separo em outra sacola e faço a compostagem', 'score': 0},
  {'text': 'Trituro o lixo e separo em outra sacola', 'score': 3}
];
