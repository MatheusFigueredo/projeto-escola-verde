class Data {
  static const List<Map<String, dynamic>> NEWS = [
    {
      "text":
          "Nós como moradores do planeta Terra, deixamos nossa marca, através da caminhada que realizamos ao longo da vida. Podemos avaliar essa \u201cmarca\u201d, como Pegada Ecológica através de um método, que avalia os padrões de consumo de uma pessoa, cidade ou país, avaliando a pressão do consumo das populações humanas sobre os recursos naturais. Podendo assim servir como guia de como utilizar os recursos do mundo com mais cuidado.\n",
      "title": "Pegada Verde",
      "type": 2,
      "urlImage": "assets/footprint/text_1.jpg"
    },
    {
      "text":
          "Surgiu em 1990 por William Rees e Mathis Wackernagel com a intenção de podermos medir o impacto que deixamos no planeta para sustentar o nosso estilo de vida.",
      "title": "Quando a pegada ecológica foi criada?",
      "type": 0,
      "urlImage": ""
    },
    {
      "text":
          "Os pesquisadores avaliam diversos tópicos como: diferentes tipos de territórios, formas de consumo, as ferramentas utilizadas e o tamanho populacional.\nObtendo todas essas informações, esses valores podem ser convertidos em uma tabela específica em uma área medida em hectare.\n\nObserva-se que locais mais industrializados têm uma pegada ecológica maior, pois tem uma população maior que acaba utilizando mais recursos que aquele território possui e assim acaba pegando recursos de outros locais. \n",
      "title": "Como é realizado o cálculo da pegada ecológica?",
      "type": 1,
      "urlImage": "assets/footprint/text_3.jpg"
    },
    {
      "text":
          "O valor da pegada ecológica está diretamente ligado ao estilo de vida. Ao ter conhecimento deste índice, podemos nos reavaliar, tanto quanto aos nossos hábitos quanto às nossas decisões, evitando o consumo em excesso de água e energia, escolha de locomoção e produção de lixo, e claro, sua reutilização. Tudo por um ambiente e vida mais saudável.",
      "title": "Como diminuir minha pegada ecológica?",
      "type": 1,
      "urlImage": "assets/footprint/text_4.jpg"
    },
  ];

  static const List<Map<String, dynamic>> PROJECT = [
    {
      "text":
          "O projeto escola verde tem como objetivo principal conscientizar a sociedade, em especial alunos de escolas públicas através da educação ambiental acerca da importância da preservação ambiental, para isso são desenvolvidas ações que promovem e estimulam a sustentabilidade. Algumas destas atividades realizadas são:",
      "title": "O projeto Escola Verde",
      "type": 0,
      "urlImage": ""
    },
    {
      "text":
          "Participantes do projeto visitam escolas e disponibilizam mudas, assim como, treinamento, orientação e acompanhamento com o intuito da escola visitada se tornar mais sustentável, além de mobilizar alunos e funcionários das escolas na arborização da mesma.",
      "title": "Arborização e jardinagem nas escolas",
      "type": 1,
      "urlImage": "assets/project/arborizacao.jpg"
    },
    {
      "text":
          "Visa incentivar alunos e funcionários das escolas visitadas a prática da coleta seletiva e reciclagem de materiais, além de incentivar o lado artístico dos alunos ao promover a produção de artesanato e obras artísticas utilizando materiais reutilizados ,através de materiais didáticos, palestras, debates e ações práticas.",
      "title": "Coleta seletiva e reciclagem nas escolas",
      "type": 1,
      "urlImage": "assets/project/reciclagem.jpg"
    },
    {
      "text":
          "Tem como objetivo sensibilizar e fomentar questões socioambientais através de atividades artísticas como dança, teatro, desenho e pintura.",
      "title": "Arte e educação ambiental",
      "type": 0,
      "urlImage": ""
    },
    {
      "text":
          "Tem como finalidade incentivar, acompanhar e orientar para que escolas incluam em seus planos de ensino a problemática socioambiental de forma interdisciplinar e permanente. Além disso, incentiva a criação de comissões de meio ambiente e qualidade de vida com a participação de alunos, professores e gestores das escolas.",
      "title": "Ambientalização",
      "type": 1,
      "urlImage": "assets/project/ambientalizacao.jpg"
    },
  ];
}
