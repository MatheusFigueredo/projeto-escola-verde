import 'package:projeto_escola_verde/components/ScoreQuiz.dart';

const List<ScoreQuiz> SCORE_QUIZ = const [
  ScoreQuiz(
    'assets/scoreQuiz/1.png',
    'Suas ações causam muito impacto ao meio ambiente! Você coloca seu planeta em risco! Mude imediatamente seus atos e hábitos, você encontrará uma vida mais saudável se o fizer e o planeta te agradecerá!',
    "Vamos reavaliar alguns hábitos. Você é negligente!",
  ),
  ScoreQuiz(
    'assets/scoreQuiz/2.png',
    'Suas ações causam impacto ao meio ambiente. Mude seus atos e hábitos, você encontrará uma vida mais saudável se o fizer e o planeta te agradecerá.',
    "Vamos diminuir essa pegada ecológica. Você é poluidor!",
  ),
  ScoreQuiz(
    'assets/scoreQuiz/3.png',
    'Suas ações causam um impacto mediano ao meio ambiente. Busque mudar seus atos e hábitos, você encontrará uma vida mais saudável se o fizer.',
    "Podemos diminuir mais essa pegada ecológica. Você é intermediário!",
  ),
  ScoreQuiz(
    'assets/scoreQuiz/4.png',
    'Parabéns! Suas ações quase não prejudicam o meio ambiente. Não precisa mudar seus hábitos mas se quiser um mundo e uma vida mais saudável, ainda há espaço para melhorias.',
    "Muito bem! Você é esforçado!",
  ),
  ScoreQuiz(
    'assets/scoreQuiz/5.png',
    'Parabéns! Suas ações não prejudicam o meio ambiente. Continue assim para garantir um mundo e uma vida saudável.',
    "Parabéns! Você é consciente!",
  ),
];
