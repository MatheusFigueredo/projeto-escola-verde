import 'package:flutter/material.dart';
import 'package:projeto_escola_verde/widgets/news/NoNewsItemList.dart';
import 'package:projeto_escola_verde/widgets/news/RightNewsItemList.dart';
import 'package:projeto_escola_verde/widgets/news/UpNewsItemList.dart';

class NewsScreen extends StatelessWidget {
  final String _name;
  final List<Map<String, dynamic>> _list;

  NewsScreen(this._name, this._list);

  @override
  Widget build(BuildContext context) {
    final newsList = _list.map((item) {
      switch (item['type']) {
        case 0:
          return NoNewsItemList(title: item['title'], text: item['text']);
        case 1:
          return UpNewsItemList(
              title: item['title'],
              urlImage: item['urlImage'],
              text: item['text']);
          break;
        case 2:
          return RightNewsItemList(
              title: item['title'],
              urlImage: item['urlImage'],
              text: item['text']);
        default:
          return NoNewsItemList(text: 'Não foi possível carregar este texto');
      }
    }).toList();
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text(
          this._name,
          style: TextStyle(
            fontFamily: "Oxygen",
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: Theme.of(context).primaryColorDark,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView.builder(
          itemCount: newsList.length,
          itemBuilder: (context, i) => newsList[i],
        ),
      ),
    );
  }
}
