import 'package:flutter/material.dart';
import 'package:projeto_escola_verde/controllers/QuizController.dart';
import 'package:projeto_escola_verde/data/ScoreQuizData.dart';

class QuizScoreScreen extends StatelessWidget {
  final _quizController = QuizController();

  @override
  Widget build(BuildContext context) {
    final _numberText = (_quizController.score == 0)?4:(_quizController.score < 42.25)?3:(_quizController.score < 84.5)?2:(_quizController.score < 126.76)?1:0;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Spacer(),
            Text(
              'Sua pegada ecológica é:',
              style: TextStyle(
                  fontFamily: 'Oxygen',
                  fontWeight: FontWeight.bold,
                  fontSize: 24),
            ),
            Spacer(),
            Text(
              '${(_quizController.score / 13).toStringAsFixed(2)}',
              style: TextStyle(fontFamily: 'Oxygen-bold', fontSize: 34),
            ),
            Spacer(),
            Container(
              padding: EdgeInsets.all(30),
              height: MediaQuery.of(context).size.height *
                  ((MediaQuery.of(context).orientation == Orientation.portrait)
                      ? 0.70
                      : 0.40),
              width: MediaQuery.of(context).size.width * 0.85,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    SCORE_QUIZ[_numberText].title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'Oxygen',
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                        fontSize: 22),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 30, bottom: 15),
                    child: Text(SCORE_QUIZ[_numberText].text,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: 'Oxygen',
                            fontWeight: FontWeight.w200,
                            color: Colors.black,
                            fontSize: 14)),
                  ),
                  Spacer(),
                  Image.asset(
                    SCORE_QUIZ[_numberText].img,
                    height: MediaQuery.of(context).size.height * 0.25,
                    fit: BoxFit.fill,
                  ),
                  Spacer(),
                ],
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Theme.of(context).primaryColorLight),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
