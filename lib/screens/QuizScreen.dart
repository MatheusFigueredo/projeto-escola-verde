import 'package:flutter/material.dart';
import 'package:projeto_escola_verde/controllers/QuizController.dart';
import 'package:projeto_escola_verde/data/QuestionsData.dart';
import 'package:projeto_escola_verde/widgets/Quiz/QuestionItem.dart';

class QuizScreen extends StatefulWidget {
  @override
  _QuizScreenState createState() => _QuizScreenState();
}

class _QuizScreenState extends State<QuizScreen> {
  QuizController _quizController = QuizController();

  @override
  Widget build(BuildContext context) {
    _quizController.onInit();
    return PageView.builder(
      physics: NeverScrollableScrollPhysics(),
      controller: _quizController.pageController,
      onPageChanged: _quizController.updateQuestionNumber,
      itemCount: QUESTIONS.length,
      itemBuilder: (contex, index) => QuestionItem(
        QUESTIONS[index],
      ),
    );
  }
}
