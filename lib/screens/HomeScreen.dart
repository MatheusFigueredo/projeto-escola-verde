import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:projeto_escola_verde/widgets/home/HomeScreenGrid.dart';
import 'package:auto_size_text/auto_size_text.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Image.asset('assets/home/logo.png',
              height: MediaQuery.of(context).size.height * 0.1),
        ),
        toolbarHeight: MediaQuery.of(context).size.height * 0.11,
        backgroundColor: Theme.of(context).primaryColorDark,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(top: 40, left: 25, right: 25, bottom: 25),
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.2,
            child: AutoSizeText(
              'A Educação Ambiental (EA) pode ser desenvolvida e vivenciada de maneiras diferentes, sobretudo nas escolas, onde o ambiente é propício para a aprendizagem, o conhecimento e a mudança de comportamento.',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Oxygen',
                fontWeight: FontWeight.w300,
                fontSize: 18,
              ),
            ),
          ),
          HomeScreenGrid(),
        ],
      ),
      bottomNavigationBar: GestureDetector(
        child: Padding(
          padding: const EdgeInsets.only(bottom: 5.0),
          child: Row(
            children: [
              Spacer(),
              Container(
                padding: EdgeInsets.all(5.0),
                child: Text(
                  'Informações dos desenvolvedores',
                  style: TextStyle(
                    fontFamily: "Oxygen",
                    fontWeight: FontWeight.w300,
                    color: Colors.white,
                  ),
                ),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColorLight,
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              Spacer(),
            ],
          ),
        ),
        onTap: () {
          Navigator.of(context).pushNamed('/info');
        },
      ),
    );
  }
}
