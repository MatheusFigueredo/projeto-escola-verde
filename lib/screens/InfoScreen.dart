import 'package:flutter/material.dart';

class InfoScreen extends StatelessWidget {
  final textStyle = TextStyle(
      fontFamily: "Oxygen", fontWeight: FontWeight.w300, color: Colors.black);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Informações dos Desenvolvedores',
          style: TextStyle(
            fontFamily: "Oxygen",
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        backgroundColor: Theme.of(context).primaryColorDark,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Organização:', style: TextStyle(fontSize: 20)),
              SizedBox(height: 10),
              RichText(
                text: TextSpan(
                  style: textStyle,
                  children: [
                    TextSpan(
                      text: 'Fernando de Jesus Bagagi',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    TextSpan(text: ' - Graduando em Engenharia da Computação')
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  style: textStyle,
                  children: [
                    TextSpan(
                      text: 'Gabriela Miranda de Souza Leite',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    TextSpan(text: ' - Graduando em Engenharia da Computação'),
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  style: textStyle,
                  children: [
                    TextSpan(
                      text: 'Matheus Figueredo Bispo',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    TextSpan(text: ' - Graduando em Engenharia da Computação'),
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  style: textStyle,
                  children: [
                    TextSpan(
                      text: 'João Victor Lopes Leal',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    TextSpan(text: ' - Graduando em Engenharia da Computação'),
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  style: textStyle,
                  children: [
                    TextSpan(
                      text: 'Ramon Barros Gomes',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    TextSpan(text: ' - Graduando em Engenharia da Computação'),
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  style: textStyle,
                  children: [
                    TextSpan(
                      text: 'Paulo Roberto Ramos',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    TextSpan(text: ' - Orientador'),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Text('Arte:', style: TextStyle(fontSize: 20)),
              SizedBox(height: 10),
              Text(
                'João Victor Lopes Leal',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              Text('Imagens:', style: TextStyle(fontSize: 20)),
              SizedBox(height: 10),
              Text(
                'Gabriela Miranda de Souza Leite',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 5),
              Text(
                'João Victor Lopes Leal',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 5),
              Text(
                'Ramon Barros Gomes',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              Text('Textos:', style: TextStyle(fontSize: 20)),
              SizedBox(height: 10),
              Text(
                'Fernando de Jesus Bagagi',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 5),
              Text(
                'Gabriela Miranda de Souza Leite',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 5),
              Text(
                'João Victor Lopes Leal',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 5),
              Text(
                'Ramon Barros Gomes',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              Text('Codificação:', style: TextStyle(fontSize: 20)),
              SizedBox(height: 10),
              Text('Fernando de Jesus Bagagi',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 5),
              Text('Gabriela Miranda de Souza Leite',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 5),
              Text('João Victor Lopes Leal',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 5),
              Text('Matheus Figueredo Bispo',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 5),
              Text('Ramon Barros Gomes',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 10),
              Text('Prototipação:', style: TextStyle(fontSize: 20)),
              SizedBox(height: 10),
              Text('Fernando de Jesus Bagagi',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 5),
              Text('Gabriela Miranda de Souza Leite',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 5),
              Text('João Victor Lopes Leal',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 5),
              Text('Matheus Figueredo Bispo',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 5),
              Text('Ramon Barros Gomes',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 10),
              Text(
                'PEVgada Ecológica. Software para explicar e calcular a pegada' +
                    'ecológica, (2021, Petrolina/PE).\n(Aplicativo para Celular) ' +
                    'Programa Escola Verde. Núcleo Temático de Educação Ambiental' +
                    ' Interdisciplinar, Junho de 2021 / Organizado por PEV-UNIVASF.' +
                    'CBL, São Paulo, 2021. 1 v. (6p.)\n\nISBN: 978-65-00-23993-5\n\n' +
                    'Tema:  Pegada Ecológica\n1.	Tela inicial.\n2. Pegada Ecológica.' +
                    '\n3. Calcular Pegada Ecológica.\n4.	Projetos.\n5. Redes Sociais.' +
                    '\n6. Informações dos Desenvolvedores'+
                    '\n\nCDD 363.70071\n\nCDU37:577.4',
              ),
              SizedBox(height: 10),
              Text(
                'Copyright © Projeto Escola Verde – Universidade Federal do Vale do São Francisco',
                style: TextStyle(fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
      ),
    );
  }
}
