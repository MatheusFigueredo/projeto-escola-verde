import 'package:flutter/material.dart';
import '../widgets/social/SocialMediaAppBar.dart';
import '../widgets/social/SocialMediaGrind.dart';

class SocialMediaScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SocialMediaAppBar(),
            SocialMediaGrind(),
          ],
        ),
      ),
    );
  }
}
