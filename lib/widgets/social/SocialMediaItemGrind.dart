import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../components/SocialMediaInfo.dart';

class SocialMediaItemGrind extends StatelessWidget {
  final SocialMediaInfo _info;

  SocialMediaItemGrind(this._info);

  void _openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(2),
      child: Padding(
        padding: EdgeInsets.all(30),
        child: TextButton(
          onPressed: () {
            _openUrl(this._info.url);
          },
          child: Image.asset(
            'assets/social/${_info.image}.png',
            height: double.infinity,
          ),
        ),
      ),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColorLight,
        borderRadius: BorderRadius.circular(200),
      ),
    );
  }
}
