import 'package:flutter/material.dart';
import '../../components/SocialMediaInfo.dart';
import 'SocialMediaItemGrind.dart';

class SocialMediaGrind extends StatelessWidget {
  final List<SocialMediaInfo> _socialMediaInfos = [
    SocialMediaInfo('facebook',
        'https://www.facebook.com/pevunivasf/'),
    SocialMediaInfo('instagram',
        'https://instagram.com/projetoescolaverde?igshid=tntwumjet6sv'),
    SocialMediaInfo('youtube',
        'https://youtube.com/c/ProjetoEscolaVerde'),
    SocialMediaInfo('twitter',
        'https://twitter.com/pevnovo'),
  ];

  @override
  Widget build(BuildContext context) {
    Size auxSize = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.all(15),
      child: GridView.count(
        crossAxisCount: (auxSize.height > auxSize.width) ? 2 : 4,
        crossAxisSpacing: 10.0,
        mainAxisSpacing: 10.0,
        shrinkWrap: true,
        children: this._socialMediaInfos.map<SocialMediaItemGrind>((item) => SocialMediaItemGrind(item)).toList(),
      ),
    );
  }
}
