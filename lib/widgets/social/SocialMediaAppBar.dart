import 'package:flutter/material.dart';

class SocialMediaAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.35,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColorDark,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(40),
          bottomRight: Radius.circular(40),
        ),
      ),
      child: Text(
        'Acesse nossas redes sociais',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontFamily: "Oxygen",
          fontWeight: FontWeight.bold,
          fontSize: 45,
          decoration: TextDecoration.none,
        ),
      ),
    );
  }
}
