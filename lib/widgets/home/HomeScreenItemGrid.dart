import 'package:flutter/material.dart';
import 'package:projeto_escola_verde/components/HomeScreenOptions.dart';

class HomeScreenItemGrid extends StatelessWidget {
  final HomeScreenOptions _item;

  HomeScreenItemGrid(this._item);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(this._item.route);
      },
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(2),
        child: LayoutBuilder(
          builder: (context, constraints) => Column(
            children: [
              Container(
                height: constraints.maxHeight * 0.8,
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 11, bottom: 0, left: 8, right: 8),
                  child: Image.asset(
                    'assets/home/${_item.image}.png',
                    height: double.infinity,
                  ),
                ),
              ),
              Container(
                child: FittedBox(
                  child: Text(
                    _item.title,
                    style: TextStyle(
                      fontFamily: "Oxygen",
                      fontWeight: FontWeight.w300,
                      color: Colors.white,
                    ),
                  ),
                ),
                height: constraints.maxHeight * 0.15,
                width: constraints.maxWidth * 0.95,
              ),
            ],
          ),
        ),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColorLight,
          borderRadius: BorderRadius.circular(20),
        ),
      ),
    );
  }
}
