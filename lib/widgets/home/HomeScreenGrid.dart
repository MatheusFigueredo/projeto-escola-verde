import 'package:flutter/material.dart';
import 'package:projeto_escola_verde/components/HomeScreenOptions.dart';
import 'HomeScreenItemGrid.dart';

class HomeScreenGrid extends StatelessWidget {
  final List<HomeScreenOptions> _homeScreenOptions = [
    HomeScreenOptions('pegada', 'Pegada Ecológica', '/news-footprint'),
    HomeScreenOptions('quiz', 'Calcular Pegada Ecológica', '/quiz'),
    HomeScreenOptions('project', 'Projetos', '/news-project'),
    HomeScreenOptions('midia', 'Redes Sociais', '/social'),
  ];

  @override
  Widget build(BuildContext context) {
    Size auxSize = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.all(30),
      child: GridView.count(
        crossAxisCount: (auxSize.height > auxSize.width) ? 2 : 4,
        crossAxisSpacing: 50,
        mainAxisSpacing: 30,
        childAspectRatio: 1.1,
        shrinkWrap: true,
        children: this
            ._homeScreenOptions
            .map<HomeScreenItemGrid>((item) => HomeScreenItemGrid(item))
            .toList(),
      ),
    );
  }
}
