import 'package:flutter/material.dart';
import 'package:projeto_escola_verde/controllers/QuizController.dart';

class QuizListItem extends StatelessWidget {
  final String text;
  final int score;
  final int index;
  QuizListItem({
    this.text,
    this.score,
    this.index,
  });

  final _quizController = QuizController();

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: _quizController.isSelectedQuestion,
      builder: (context, value, child) {
        return InkWell(
          onTap: !_quizController.isSelectedQuestion.value
              ? () {
                  _quizController.nextQuestion(score, index);
                }
              : null,
          child: Container(
            margin: EdgeInsets.all(
                _quizController.selectedQuestionIndex.value == index
                    ? _quizController.marginOptionItem.value
                    : 8),
            //margin: EdgeInsets.symmetric(vertical: 8),
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColorLight,
              borderRadius: BorderRadius.circular(20),
              border: _quizController.selectedQuestionIndex.value == index
                  ? Border.all(color: Colors.black, width: 3)
                  : null,
            ),
            child: Text(
              text,
              style: TextStyle(
                fontFamily: 'Oxygen',
                fontWeight: FontWeight.bold,
                color: Colors.white,
                fontSize: 15,
              ),
            ),
          ),
        );
      },
    );
  }
}
