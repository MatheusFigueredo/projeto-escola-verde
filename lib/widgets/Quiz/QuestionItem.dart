import 'package:flutter/material.dart';
import 'package:projeto_escola_verde/components/Question.dart';
import 'QuestionGreenCard.dart';
import 'QuizOptionItem.dart';

class QuestionItem extends StatelessWidget {
  final Question question;

  QuestionItem(this.question);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: [
            Spacer(),
            QuestionGreenCard(
              imgUrl: question.img,
              title: question.title,
              text: question.text,
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              height: MediaQuery.of(context).size.height * 0.45,
              child: ListView.builder(
                itemCount: 4,
                //shrinkWrap: true,
                itemBuilder: (ctx, index) => QuizListItem(
                  text: question.options[index]['text'],
                  score: question.options[index]['score'],
                  index: index,
                ),
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}

//GridView.count(
//   crossAxisCount: 2,
//   crossAxisSpacing: 10,
//   mainAxisSpacing: 10,
//   childAspectRatio: 1.3,
//   shrinkWrap: true,
//   children: List.generate(
//     4,
//     (index) => Container(
//       color: Colors.black,
//       child: Text(
//         '$index',
//         style: TextStyle(
//           color: Colors.white,
//         ),
//       ),
//     ),
//   ),
// ),
