import 'package:flutter/material.dart';

class QuestionGreenCard extends StatelessWidget {
  final String imgUrl;
  final String title;
  final String text;

  const QuestionGreenCard({this.imgUrl, this.title, this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.45,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColorDark,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Padding(
        padding: const EdgeInsets.all(15),
        child: LayoutBuilder(
          builder: (context, constraints) {
            return Column(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: Image.asset(
                    imgUrl,
                    fit: BoxFit.fill,
                    height: constraints.maxHeight * 0.68,
                    width: double.infinity,
                  ),
                ),
                Spacer(),
                /*Container(
                  height: constraints.maxHeight * 0.10,
                  width: double.infinity,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Text(
                      title,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),*/
                Text(
                  text,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontFamily: 'Oxygen',
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                  softWrap: true,
                ),
                /*Container(
                  height: constraints.maxHeight * 0.30,
                  width: double.infinity,
                  child: Text(
                    text,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                    textAlign: TextAlign.justify,
                    softWrap: true,
                  ),
                ),*/
                Spacer()
              ],
            );
          },
        ),
      ),
    );
  }
}
