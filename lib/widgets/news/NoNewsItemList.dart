import 'package:flutter/material.dart';

import 'NewsItemList.dart';

class NoNewsItemList extends NewsItemList {
  NoNewsItemList({String title, String text}) : super(title, '', text);

  @override
  Widget build(BuildContext context) {
    return super.wrap(
      context,
      this.title.isEmpty
          ? Text(
              this.text,
              style: super.textStyle,
            )
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(this.title, style: super.titleStyle),
                SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                Text(
                  this.text,
                  style: super.textStyle,
                ),
              ],
            ),
    );
  }
}
