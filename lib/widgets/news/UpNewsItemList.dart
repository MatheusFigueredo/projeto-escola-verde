import 'package:flutter/material.dart';
import 'package:projeto_escola_verde/widgets/news/NewsItemList.dart';

class UpNewsItemList extends NewsItemList {
  UpNewsItemList({String title = '', String urlImage, String text})
      : super(title, urlImage, text);

  @override
  Widget build(BuildContext context) {
    return super.wrap(
      context,
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (this.title.isNotEmpty) Text(this.title, style: super.titleStyle),
          SizedBox(height: MediaQuery.of(context).size.height * 0.02),
          Image.asset(this.urlImage, width: double.infinity),
          SizedBox(height: MediaQuery.of(context).size.height * 0.02),
          Text(this.text, style: super.textStyle),
        ],
      ),
    );
  }
}
