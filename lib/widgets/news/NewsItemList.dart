import 'package:flutter/material.dart';

abstract class NewsItemList extends StatelessWidget {
  final String _title;
  final String _urlImage;
  final String _text;
  final TextStyle _titleStyle = new TextStyle(
      fontSize: 28, fontFamily: 'Oxygen', fontWeight: FontWeight.bold);
  final TextStyle _textStyle = new TextStyle(
      fontSize: 15, fontFamily: 'Oxygen', fontWeight: FontWeight.w400);

  NewsItemList(this._title, this._urlImage, this._text);

  String get title => this._title;
  String get urlImage => this._urlImage;
  String get text => this._text;
  TextStyle get titleStyle => this._titleStyle;
  TextStyle get textStyle => this._textStyle;

  Widget wrap(BuildContext context, Widget child) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding:
            const EdgeInsets.only(left: 10, bottom: 5, top: 15, right: 10),
        child: child,
      ),
    );
  }

  @override
  Widget build(BuildContext context);
}
