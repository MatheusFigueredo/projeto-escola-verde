import 'package:flutter/material.dart';
import 'package:projeto_escola_verde/widgets/news/NewsItemList.dart';

class RightNewsItemList extends NewsItemList {
  RightNewsItemList({String title = '', String urlImage, String text})
      : super(title, urlImage, text);

  @override
  Widget build(BuildContext context) {
    final aux = Row(
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.6,
          padding: EdgeInsets.only(right: 10.0),
          child: Text(
            this.text,
            //textAlign: TextAlign.justify,
            overflow: TextOverflow.clip,
            style: super.textStyle,
          ),
        ),
        Image.asset(
          this.urlImage,
          width: MediaQuery.of(context).size.width * 0.25,
        ),
      ],
    );
    return super.wrap(
      context,
      this.title.isEmpty
          ? aux
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  this.title,
                  style: super.titleStyle,
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                aux,
              ],
            ),
    );
  }
}
