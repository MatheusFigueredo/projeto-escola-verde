import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:projeto_escola_verde/screens/HomeScreen.dart';
import 'package:projeto_escola_verde/screens/InfoScreen.dart';
import 'package:projeto_escola_verde/screens/QuizScreen.dart';
import 'package:projeto_escola_verde/screens/SocialMediaScreen.dart';
import 'package:projeto_escola_verde/screens/NewsScreen.dart';
import 'data/Data.dart';

void main() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown
  ]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Pegada Verde',
      theme: ThemeData(
        primarySwatch: Colors.green,
        primaryColor: Color(0xFFE8E9C3),
        primaryColorLight: Color(0xFF70C0A2),
        primaryColorDark: Color(0xFF4D9413),
      ),
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
      routes: {
        '/home': (context) => HomeScreen(),
        '/quiz': (context) => QuizScreen(),
        '/news-footprint': (context) =>
            NewsScreen('O que é Pegada Ecológica', Data.NEWS),
        '/news-project': (context) => NewsScreen('Projetos', Data.PROJECT),
        '/social': (context) => SocialMediaScreen(),
        '/info': (context) => InfoScreen(),
      },
    );
  }
}
