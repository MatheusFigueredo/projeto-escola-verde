import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:projeto_escola_verde/data/QuestionsData.dart';
import 'package:projeto_escola_verde/screens/QuizScoreScreen.dart';

class QuizController extends GetxController {
  static final QuizController _singleton = QuizController._internal();

  factory QuizController() {
    return _singleton;
  }

  QuizController._internal();

  PageController _pageController = PageController();
  PageController get pageController => this._pageController;

  int _score = 0;
  int get score => _score;

  int _questionNumber = 1;
  int get questionNumber => this._questionNumber;

  ValueNotifier<bool> _isSelectedQuestion = ValueNotifier(false);
  ValueNotifier<bool> get isSelectedQuestion => _isSelectedQuestion;

  ValueNotifier<int> _selectedQuestionIndex = ValueNotifier(5);
  ValueNotifier<int> get selectedQuestionIndex => _selectedQuestionIndex;

  ValueNotifier<double> _marginOptionItem = ValueNotifier(10.0);
  ValueNotifier<double> get marginOptionItem => _marginOptionItem;

  nextQuestion(int questionScore, int index) {
    _score += questionScore;
    isSelectedQuestion.value = true;
    _selectedQuestionIndex.value = index;
    _marginOptionItem.value = 7.0;
    Future.delayed(Duration(milliseconds: 500), () {
      if (_questionNumber != QUESTIONS.length) {
        _marginOptionItem.value = 10.0;
        isSelectedQuestion.value = false;
        _pageController.nextPage(
            duration: Duration(milliseconds: 250), curve: Curves.ease);
        _selectedQuestionIndex.value = 5;
      } else {
        isSelectedQuestion.value = false;
        Get.off(() => QuizScoreScreen());
      }
    });
  }

  updateQuestionNumber(int index) {
    _questionNumber = index + 1;
  }

  @override
  void onInit() {
    super.onInit();
    _score = 0;
    _questionNumber = 1;
    _selectedQuestionIndex.value = 5;
    _marginOptionItem.value = 10;
    _isSelectedQuestion.value = false;
  }
}
